#!/usr/bin/env bash

alias l='ls -la'
alias ll='ls -lah'
alias lt='ls -lat'
alias hidden='ls -dl .*' # List hidden
alias ~='cd ~'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

alias show_syslog="tail -f /var/log/system.log"
alias show_ports_used="sudo lsof -i -P | grep -i \"listen\""
alias show_proxy_status="scutil --proxy"
alias show_ip_address="ipconfig getifaddr en0"

alias flush_dns="sudo killall -HUP mDNSResponder"

# reload .bash_profile
alias reload=". $HOME/.bash_profile"

# edit bash profile shortcuts
alias bp="vim $HOME/.bash_profile"
alias ba="vim $HOME/.bash/.bash_aliases"
alias bf="vim $HOME/.bash/.bash_functions"
alias be="vim $HOME/.bash/.bash_environment"
alias bpxy="vim $HOME/.bash/.bash_proxy"

# NPM
alias npm_list_config="npm config ls -l"
alias npm_list_global="npm -g ls --depth=0"

