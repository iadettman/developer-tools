#!/usr/bin/env bash

MAVEN_2_1_HOME="$HOME/java/apache-maven-3.2.1"
MAVEN_2_HOME="$HOME/java/apache-maven-3.2.5"
MAVEN_3_HOME="$HOME/java/apache-maven-3.3.9"

DEV_MODE_SAGE="SAGE"
DEV_MODE_EONLINE="EONLINE"

function set_java_version(){
	export JAVA_HOME=`/usr/libexec/java_home -v 1.$1`
}

function set_maven_home(){
	export MAVEN_HOME=$1
	export PATH="$PERSISTENT_PATH:$MAVEN_HOME/bin"
}

function update_java_env(){
	set_java_version $1
	set_maven_home $2
	mvn -version
}

function update_dev_env(){
	if test "$1" == "${DEV_MODE_SAGE}"
	then
		update_java_env 7 ${MAVEN_2_HOME}
	else
		update_java_env 8 ${MAVEN_3_HOME}
	fi
}

function env_sage(){
	update_dev_env "${DEV_MODE_SAGE}"
}

function env_eonline(){
	update_dev_env "${DEV_MODE_EONLINE}"
}

# call default env function on bash profile load
env_sage
