#!/usr/bin/env bash


EONLINE_COMMON="$HOME/dev/eonline/eonline-commons-client/pom.xml"
EONLINE_SERVICES="$HOME/dev/eonline/eonline-service/pom.xml"

alias build_eonline_deps="mvn -f $EONLINE_COMMON -DskipTests clean install && mvn -f $EONLINE_SERVICES -DskipTests clean install"
