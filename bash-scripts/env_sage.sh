#!/usr/bin/env bash

SAGE_PROJECT_HOME="$HOME/dev/sage"

SAGE="$HOME/dev/sage/mts-sage"

SAGE_JBOSS="${SAGE_PROJECT_HOME}/sage-jboss/jboss-eap-4.3/jboss-as"

SAGE_JBOSS_DEPLOY_DIR="${SAGE_JBOSS}/server/sage"

SAGE_JBOSS_PROFILE="sage"

alias cdsage="cd ${SAGE}"
alias cdjboss="cd ${SAGE_JBOSS}"

alias sage_build="mvn -DskipTests clean install dependency:copy"
alias sage_build_module="mvn -DskipTests install -pl sage-war -am dependency:copy"

alias sage_server="${SAGE_JBOSS}/bin/run.sh -b 0.0.0.0 -c ${SAGE_JBOSS_PROFILE}"

JUMPBOXPW="GJCnhQBI"
JUMPBOX_USER='idettman@100.122.86.131'
JUMPBOX_MYSQL='3306:ptddbeolauth401.ptg.inbcu.com:3306'
JUMPBOX_MYSQL2='3307:ptddbeol401.ptg.inbcu.com:3306'
JUMPBOX_MONGO='27018:eoddbmongo401.eol.inbcu.com:27017'

alias sage_jumpbox_portforwarding="ssh -t -L ${JUMPBOX_MYSQL} -L ${JUMPBOX_MYSQL2} -L ${JUMPBOX_MONGO} ${JUMPBOX_USER}"