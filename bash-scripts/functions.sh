#!/usr/bin/env bash

function zip_folder(){
	zip -X -r "$@.zip" "$@" 
}


# USER

function show_group_membership(){
	dscl . search /Groups GroupMembership "$@" | grep = | awk '{print $1}'
}

function show_group(){
	groups "$@" | cut -d' ' -f1
}

# NPM

function npm_list_tags(){
	npm dist-tag ls "$@"
}

function npm_list_dependencies(){
	npm view "$@" dependencies
}

function npm_list_package_options(){
	npm view opts@$(npm view "$@" dependencies.opts)
}

function npm_list_package_version(){
	npm view "$@" versions
}

function npm_list_all(){
	for package in $(npm -g ls --depth=0 --parseable); do
		printf "${package##*/}\n";
	done
}