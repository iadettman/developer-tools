#!/usr/bin/env bash

# FLAGS

# o - list long (GROUP OMITTED)

# c - sort by time last modified

# A - show all except for . and ...

# G - colorized output
# F - show '/' after directories and '*' after executables
# p - show '/' after directories

# e - show ACL

alias l='ls -lAhGpc'
alias ll='ls -lahGp'
alias lt='ls -lAt'

# List directories first
alias ld='ls -oap | grep "^d" && ls -la | grep -v "^d"'

# List hidden
alias hidden='ls -dl .*'

alias ~='cd ~'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

alias show_syslog="tail -f /var/log/system.log"
alias show_ports_used="sudo lsof -i -P | grep -i \"listen\""
alias show_proxy_status="scutil --proxy"
alias show_ip_address="ipconfig getifaddr en0"

alias flush_dns="sudo killall -HUP mDNSResponder"

# reload .bash_profile
alias reload=". $HOME/.bash_profile"

# NPM
alias npm_list_config="npm config ls -l"
alias npm_list_global="npm -g ls --depth=0"

