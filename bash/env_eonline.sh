#!/usr/bin/env bash

EONLINE_COMMON="$HOME/dev/eonline/eonline-commons-client"
EONLINE_SERVICE="$HOME/dev/eonline/eonline-service"
EONLINE_MOBILE="$HOME/dev/eonline/eonline-mobile"
EONLINE_DESKTOP="$HOME/dev/eonline/eonline-desktop"

EONLINE_COMMON_POM="$EONLINE_COMMON/pom.xml"
EONLINE_SERVICE_POM="$EONLINE_SERVICE/pom.xml"

# MAVEN NOTE - use '-f' flag to build without changing to project directory

alias build_eonline_deps="mvn -f $EONLINE_COMMON_POM -DskipTests clean install && mvn -f $EONLINE_SERVICE_POM -DskipTests clean install"

