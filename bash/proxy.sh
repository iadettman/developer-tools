#!/usr/bin/env bash

function proxy_on(){
	local proxy_value='proxy.inbcu.com:80'
	local no_proxy_value='local,*.local,169.254/16,*.169.254/16,tfayd.com,*.tfayd.com,nbcuni.ge.com,*.nbcuni.ge.com,inbcu.com,*.inbcu.com,mail.tfayd.com,*.mail.tfayd.com,stg-tfayd.com,*.stg-tfayd.com,e2k.ad.ge.com,*.e2k.ad.ge.com,cns.comcastnets.com,*.cns.comcastnets.com,ceg.comcastnets.com,*.ceg.comcastnets.com,tgcinc.com,*.tgcinc.com,eentertainment.com,*.eentertainment.com,cable.comcast.com,*.cable.comcast.com,corphq.comcast.com,*.corphq.comcast.com,comcastnets.com,*.comcastnets.com,eng.interplay3.prod,*.eng.interplay3.prod,udh.unistudios.com,*.udh.unistudios.com,interactive.msnbc.com,*.interactive.msnbc.com'
	
	export http_proxy=${proxy_value}
	export https_proxy=${proxy_value}
	export no_proxy=${no_proxy_value}
	
	export HTTP_PROXY=${proxy_value}
	export HTTPS_PROXY=${proxy_value}
	export NO_PROXY=${no_proxy_value}
}

function proxy_off(){
	local proxy_value=''
	local no_proxy_value=''
	
	export http_proxy=${proxy_value}
	export https_proxy=${proxy_value}
	export no_proxy=${no_proxy_value}
	
	export HTTP_PROXY=${proxy_value}
	export HTTPS_PROXY=${proxy_value}
	export NO_PROXY=${no_proxy_value}
}